resource "aws_security_group" "backend-ec2-elb-access3" {
  name        = "ec2-elb-access3"
  description = "Grants SSH access to EC2 instances through the Load Balancer"
  vpc_id      = "vpc-05cd24fbd78e24d4c"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    self        = false
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "backend-elb-public3" {
  name        = "elb-public"
  description = "Grants http traffic through ELB"
  vpc_id      = "vpc-05cd24fbd78e24d4c"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ec2-rds-access3" {
  name        = "rds-access3"
  description = "Grants EC2 instances access to the RDS instances"
  vpc_id      = "vpc-05cd24fbd78e24d4c"

  ingress {
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = ["${aws_security_group.backend-ec2-elb-access3.id}"]
    self            = false
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "ec2_rds_security_group" {
  value = "${aws_security_group.ec2-rds-access3.id}"
}
