
variable "associate_public_ip_address" {
  default     = "true"
  description = "Specifies whether to launch instances in your VPC with public IP addresses."
}

# variable "ssl_cert_arn" {}

# variable "https_port" {}
