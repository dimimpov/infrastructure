resource "aws_iam_role" "backend_beanstalk_role" {
  name               = "beanstalk-role2"
  assume_role_policy = "${file("policy.json")}"
}

resource "aws_iam_role_policy_attachment" "beanstalk_service" {
  role       = "${aws_iam_role.backend_beanstalk_role.id}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkService"
}

resource "aws_iam_role_policy_attachment" "beanstalk_service_health" {
  role       = "${aws_iam_role.backend_beanstalk_role.id}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSElasticBeanstalkEnhancedHealth"
}

# -- IAM role for EC2 & relative iam instance profile & policy attachments
resource "aws_iam_role" "backend_beanstalk_ec2_2" {
  name               = "beanstalk-ec2_2"
  assume_role_policy = "${file("ec2.json")}"
}

resource "aws_iam_instance_profile" "beanstalk-ec2-profile_2_2" {
  name = "beanstalk-ec2-profile3"
  role = "${aws_iam_role.backend_beanstalk_ec2_2.name}"
}

resource "aws_iam_role_policy_attachment" "beanstalk_ec2_web_2" {
  role       = "${aws_iam_role.backend_beanstalk_ec2_2.id}"
  policy_arn = "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier"
}
