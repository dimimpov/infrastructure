provider "aws" {
  region = "us-east-2"
}

# resource "aws_security_group" "allow_tls" {
#   name        = "allow_tls"
#   description = "Allow TLS inbound traffic"
#   vpc_id      = "${aws_vpc.main.id}"

#   ingress {
#     # TLS (change to whatever ports you need)
#     from_port = 443
#     to_port   = 443
#     protocol  = "-1"
#     # Please restrict your ingress to only necessary IPs and ports.
#     # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
#   }

#   egress {
#     from_port       = 0
#     to_port         = 0
#     protocol        = "-1"
#     cidr_blocks     = ["0.0.0.0/0"]
#     prefix_list_ids = ["pl-12c4e678"]
#   }
# }



resource "aws_instance" "web" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  key_name      = "testing2"

  tags = {
    Name = "backend"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
resource "aws_db_instance" "db_instance2" {
  allocated_storage      = 5
  max_allocated_storage  = 10
  vpc_security_group_ids = ["${aws_security_group.ec2-rds-access3.id}"]
  db_subnet_group_name   = "${aws_db_subnet_group.db-subnet-group.name}"
  availability_zone      = "us-east-2b"



  storage_type   = "gp2"
  engine         = "postgres"
  engine_version = "10.6"
  instance_class = "db.t2.micro"
  name           = "nodeapi2"
  username       = "dimitrios"
  password       = "king2306"

}

resource "aws_db_subnet_group" "db-subnet-group" {
  name        = "subnetsrds"
  description = "subnets for RDS"
  subnet_ids  = ["subnet-09c4d9c5e5d6e3504", "subnet-0376f0bf6da87ceae"]
}

resource "aws_elastic_beanstalk_application" "beanstalk-app" {
  name        = "backend-app2"
  description = "Backend app dimitrios"
}
resource "aws_route53_zone" "hosted_node" {
  name = "dimimpovcloud.com"
}

resource "aws_route53_record" "route53LB" {
  zone_id = "${aws_route53_zone.hosted_node.zone_id}"
  name    = "api.dimimpovcloud.com"
  type    = "A"

  alias {
    name                   = "awseb-e-x-AWSEBLoa-1TKWZ28J1SPWU-2117796348.us-east-2.elb.amazonaws.com"
    zone_id                = "Z3AADJGX6KTTL2"
    evaluate_target_health = true
  }
}


resource "aws_elastic_beanstalk_environment" "django-environment-test234" {
  name                   = "django-env-test34"
  application            = "${aws_elastic_beanstalk_application.beanstalk-app.name}"
  solution_stack_name    = "64bit Amazon Linux 2018.03 v2.8.3 running Python 3.6"
  wait_for_ready_timeout = "10m"
  poll_interval          = "15s"


  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "${aws_iam_role.backend_beanstalk_role.name}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = "${var.associate_public_ip_address}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:healthreporting:system"
    name      = "SystemType"
    value     = "enhanced"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "${aws_iam_instance_profile.beanstalk-ec2-profile_2_2.name}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = "${aws_security_group.backend-ec2-elb-access3.id}, ${aws_security_group.ec2-rds-access3.id},"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateType"
    value     = "Health"
  }

  setting {
    namespace = "aws:autoscaling:updatepolicy:rollingupdate"
    name      = "RollingUpdateEnabled"
    value     = "true"
  }


  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "CrossZone"
    value     = "true"
  }
  setting {
    namespace = "aws:elb:policies"
    name      = "ConnectionDrainingEnabled"
    value     = "true"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = "vpc-05cd24fbd78e24d4c"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "subnet-09c4d9c5e5d6e3504,subnet-0376f0bf6da87ceae"
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "ELBSubnets"
    value     = "subnet-09c4d9c5e5d6e3504"
  }
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "${aws_iam_instance_profile.beanstalk-ec2-profile_2_2.name}"
  }
  # setting {
  #   namespace = "aws:elb:listener"
  #   name      = "ListenerProtocol"
  #   value     = "HTTPS"
  # }

  setting {
    namespace = "aws:elb:listener"
    name      = "InstanceProtocol"
    value     = "HTTP"
  }


  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "SecurityGroups"
    value     = "${aws_security_group.backend-elb-public3.id}"
  }

  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "ManagedSecurityGroup"
    value     = "${aws_security_group.backend-elb-public3.id}"
  }
  # setting {
  #   namespace = "aws:elb:loadbalancer"
  #   name      = "LoadBalancerHTTPSPort"
  #   value     = "${var.https_port}"
  # }


  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "EC2KeyName"
    value     = "django-backend"
  }
  # setting {
  #   namespace = "aws:autoscaling:launchconfiguration"
  #   name      = "EC2KeyName"
  #   value     = "${var.app_name}"
  # }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "AssociatePublicIpAddress"
    value     = "true"
  }

  setting {
    namespace = "aws:elb:listener"
    name      = "SSLCertificateId"
    value     = "arn:aws:acm:us-east-2:058227920454:certificate/41450085-21d7-4e77-b897-1175dcff37d7"
  }

  setting {
    namespace = "aws:autoscaling:asg"
    name      = "Availability Zones"
    value     = "Any 2"
  }

  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "LoadBalancerHTTPPort"
    value     = "80"
  }


  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "InstanceType"
    value     = "t2.micro"
  }

}
