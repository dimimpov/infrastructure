resource "aws_security_group" "ec2-redis-access" {
  name        = "${var.environment}-ec2-redis-access"
  description = "Grants EC2 instances access to the Redis cluster"
  vpc_id      = "vpc-754f571d"

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    # security_groups = [""]
    self = false
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
