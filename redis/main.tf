provider "aws" {
  region = "us-east-2"
}

resource "aws_elasticache_replication_group" "redis-cluster" {
  replication_group_id          = "${var.environment}"
  replication_group_description = "${var.environment} redis cluster"
  node_type                     = "cache.m4.large"
  port                          = 6379
  parameter_group_name          = "${var.parameter_group_name}"
  automatic_failover_enabled    = true
  maintenance_window            = "sun:05:00-sun:09:00"
  apply_immediately             = true
  transit_encryption_enabled    = false
  at_rest_encryption_enabled    = true
  engine_version                = "${var.engine_version}"

  availability_zones = ["us-east-2a", "us-east-2b"]
  security_group_ids = ["${aws_security_group.ec2-redis-access.id}"]
  cluster_mode {
    replicas_per_node_group = "1"
    num_node_groups         = "1"
  }
}
