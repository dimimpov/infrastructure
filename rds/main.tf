provider "aws" {
  region = "us-east-2"
}
resource "aws_db_subnet_group" "db-subnet-group" {
  name        = "subnetsrds"
  description = "subnets for RDS"
  subnet_ids  = ["subnet-77110b1f", "subnet-f76a3a8d"]
}
resource "aws_db_instance" "test" {
  allocated_storage      = 5
  max_allocated_storage  = 10
  vpc_security_group_ids = ["${aws_security_group.lambda-rds-access.id}"]
  db_subnet_group_name   = "${aws_db_subnet_group.db-subnet-group.name}"


  storage_type        = "gp2"
  engine              = "postgres"
  engine_version      = "10.6"
  instance_class      = "db.t2.micro"
  name                = "test"
  username            = "test"
  password            = "testtest"
  publicly_accessible = true
}
