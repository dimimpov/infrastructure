resource "aws_security_group" "lambda-rds-access" {
  name        = "rds-access3"
  description = "Grants LAMBDA  access to the RDS instances"
  vpc_id      = "vpc-e7aa588c"

  ingress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    self      = false
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
