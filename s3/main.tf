resource "aws_s3_bucket" "forparik3" {
  bucket = "forparik3"
}

resource "aws_s3_bucket_public_access" "forparik3" {
  bucket = "${aws_s3_bucket.example.id}"

  block_public_acls   = false
  block_public_policy = false
}

resource "aws_s3_bucket_object" "object" {
  bucket = "forparik3"
  key    = "new_object_key"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
}
