provider "aws" {
  region = "us-east-2"
}

module "test-cluster" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_name = "testing-cluster4"
  subnets      = ["subnet-2a0b0b42", "subnet-5b05a317"]
  vpc_id       = "vpc-754f571d"

  worker_groups = [
    {
      instance_type = "m4.large"
      asg_max_size  = 5

    }
  ]

  tags = {
    environment = "testing"
  }
}
