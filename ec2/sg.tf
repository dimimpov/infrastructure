resource "aws_security_group" "ec2-ssh-access" {
  name        = "ec2-ssh-access"
  description = "Grants SSH access to EC2 instance"
  vpc_id      = "vpc-e7aa588c"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    self        = false
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
