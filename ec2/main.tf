provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "testingServer" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  key_name               = "ec2Key"
  vpc_security_group_ids = ["${aws_security_group.ec2-ssh-access.id}"] #add redis sg later

}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}
