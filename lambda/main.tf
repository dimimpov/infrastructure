provider "aws" {
  region = "us-east-2"
}
resource "aws_sns_topic" "topic" {
  name = "s3-event-notification-topic"

  policy = <<POLICY
{
    "Version":"2012-10-17",
    "Statement":[{
        "Effect": "Allow",
        "Principal": {"AWS":"*"},
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:*:*:s3-event-notification-topic",
        "Condition":{
            "ArnLike":{"aws:SourceArn":"${aws_s3_bucket.bucket.arn}"}
        }
    }]
}
POLICY
}

resource "aws_iam_role" "goScrapper" {
  name               = "goScrapper"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": {
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "lambda.amazonaws.com"
    },
    "Effect": "Allow"
  }
}
POLICY
}

resource "aws_s3_bucket" "bucket" {
  bucket = "dimimpovbucket"
}
resource "aws_s3_bucket_policy" "b" {
  bucket = "${aws_s3_bucket.bucket.id}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "MYBUCKETPOLICY",
  "Statement": [
    {
      "Effect": "Allow",
        "Action": [
            "s3:*"
        ],
        "Principal": {"AWS":"*"},
        "Resource": [
            "arn:aws:s3:::dimimpovbucket",
            "arn:aws:s3:::dimimpovbucket/*"
        ]
    }
  ]
}
POLICY
}
resource "aws_s3_bucket_object" "lambda-package3" {
  bucket = "${aws_s3_bucket.bucket.id}"
  key    = "create.zip"
  source = "./create.zip"
}
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.bucket.id}"

  topic {
    topic_arn     = "${aws_sns_topic.topic.arn}"
    events        = ["s3:ObjectCreated:*"]
    filter_suffix = ".log"
  }
}

resource "aws_lambda_function" "create" {
  function_name = "search"
  s3_bucket     = "${aws_s3_bucket.bucket.bucket}"
  s3_key        = "${aws_s3_bucket_object.lambda-package3.key}"
  handler       = "main"
  #this is the build file from go
  #   source_code_hash = "${filebase64sha256("create.zip")}"
  role        = "${aws_iam_role.goScrapper.arn}"
  runtime     = "go1.x"
  memory_size = 128
  timeout     = 300

  vpc_config = {
    subnet_ids         = ["subnet-77110b1f", "subnet-f76a3a8d"]
    security_group_ids = ["${var.security_group_from_rds}"]
  }
}


resource "aws_lambda_permission" "goScrapper" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.create.arn}"
  principal     = "apigateway.amazonaws.com"
}

resource "aws_api_gateway_resource" "goScrapper" {
  rest_api_id = "${aws_api_gateway_rest_api.goScrapper.id}"
  parent_id   = "${aws_api_gateway_rest_api.goScrapper.root_resource_id}"
  path_part   = "goScrapper"
}

resource "aws_api_gateway_rest_api" "goScrapper" {
  name = "goScrapper"
}

resource "aws_api_gateway_method" "goScrapper" {
  rest_api_id   = "${aws_api_gateway_rest_api.goScrapper.id}"
  resource_id   = "${aws_api_gateway_resource.goScrapper.id}"
  http_method   = "GET"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "goScrapper" {
  rest_api_id             = "${aws_api_gateway_rest_api.goScrapper.id}"
  resource_id             = "${aws_api_gateway_resource.goScrapper.id}"
  http_method             = "${aws_api_gateway_method.goScrapper.http_method}"
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "${aws_lambda_function.create.invoke_arn}"
}
resource "aws_api_gateway_deployment" "goScrapper_v1" {
  depends_on = [
    "aws_api_gateway_integration.goScrapper"
  ]
  rest_api_id = "${aws_api_gateway_rest_api.goScrapper.id}"
  stage_name  = "v1"
}
